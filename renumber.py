from fileinput import FileInput
import click

@click.command()
@click.argument('path', type=click.Path(exists=True))

def renumber(path):
	with FileInput(path, inplace=True) as f:
		i = 1
		piece = '\"execution_count\": '
		for line in f:
			if line.startswith(' '*3 + piece):
				print(' '*3 + piece + str(i) + ',')
				i += 1
			elif line.startswith(' '*5 + piece):
				print(' '*5 + piece + str(i - 1) + ',')
			else:
				print(line, end='')

if __name__ == "__main__":
    renumber()